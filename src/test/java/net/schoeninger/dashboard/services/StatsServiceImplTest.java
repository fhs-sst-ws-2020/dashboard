package net.schoeninger.dashboard.services;

import net.schoeninger.dashboard.domain.Stats;
import net.schoeninger.dashboard.domain.Task;
import net.schoeninger.dashboard.repositories.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class StatsServiceImplTest {

    private StatsServiceImpl service;

    @Mock
    private TaskRepository taskRepository;

    @BeforeEach
    void setUp() {
        service = new StatsServiceImpl(taskRepository);
    }

    @Test
    void getStatsForProject_ok() {
        // Mock:
        Mockito.when(taskRepository.findAllDoneTasksByProjectId(1)).thenReturn(
            Arrays.asList(
                Task.builder().timeEstimateMinutes(100).actualTimeMinutes(50).build(),
                Task.builder().timeEstimateMinutes(100).actualTimeMinutes(125).build(),
                Task.builder().timeEstimateMinutes(0).actualTimeMinutes(125).build()));
        // Execute test:
        Stats stats = service.getStatsForProject(1);
        // Assertions:
        assertNotNull(stats);
        assertEquals(3, stats.getNumberOfTasks());
        assertEquals(0.65, stats.getQualityOfEstimates());
    }

    @Test
    void getStatsForProject_taskListEmpty() {
        // Mock:
        Mockito.when(taskRepository.findAllDoneTasksByProjectId(1)).thenReturn(new ArrayList<>());
        // Execute test:
        Stats stats = service.getStatsForProject(1);
        // Assertions:
        assertNotNull(stats);
        assertEquals(0, stats.getNumberOfTasks());
        assertEquals(-1, stats.getQualityOfEstimates());
    }

    @Test
    void getStatsForProject_taskListNull() {
        // Mock:
        Mockito.when(taskRepository.findAllDoneTasksByProjectId(1)).thenReturn(null);
        // Execute test & assert:
        assertThrows(NoSuchElementException.class, () -> service.getStatsForProject(1));
    }

}
