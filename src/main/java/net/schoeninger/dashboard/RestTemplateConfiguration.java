package net.schoeninger.dashboard;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Configuration
public class RestTemplateConfiguration {

    @Profile("!kube")
    @LoadBalanced
    @Bean
    public RestTemplate restTemplateEureka() {
        log.info("Created RestTemplate using @LoadBalanced for Eureka.");
        return new RestTemplate();
    }

    @Profile("kube")
    @Bean
    public RestTemplate restTemplate() {
        log.info("Created RestTemplate.");
        return new RestTemplate();
    }

}
