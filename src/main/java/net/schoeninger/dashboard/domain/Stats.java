package net.schoeninger.dashboard.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor @Getter @Builder
public class Stats {

    private double qualityOfEstimates;
    private int numberOfTasks;

}
