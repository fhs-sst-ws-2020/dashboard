package net.schoeninger.dashboard.domain;

import lombok.*;

@AllArgsConstructor @NoArgsConstructor @Getter @Setter @Builder
public class Task {

    private int timeEstimateMinutes;
    private int actualTimeMinutes;

}
