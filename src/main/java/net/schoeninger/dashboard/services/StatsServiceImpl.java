package net.schoeninger.dashboard.services;

import net.schoeninger.dashboard.domain.Stats;
import net.schoeninger.dashboard.domain.Task;
import net.schoeninger.dashboard.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StatsServiceImpl implements StatsService {

    private TaskRepository taskRepository;

    @Autowired
    public StatsServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Stats getStatsForProject(long projectId) {
        List<Task> tasks = taskRepository.findAllDoneTasksByProjectId(projectId);
        if (tasks == null) {
            throw new NoSuchElementException("Tasks for projectId " + projectId + " null.");
        }
        double qualityOfEstimates = calculateQuality(tasks);
        return Stats.builder().qualityOfEstimates(qualityOfEstimates).numberOfTasks(tasks.size()).build();
    }

    private double calculateQuality(List<Task> tasks) {
//        int counter = 0;
//        double sum = 0;
//        for (Task task: tasks) {
//            if (task.getActualTimeMinutes() > 0 && task.getTimeEstimateMinutes() > 0) {
//                sum += calculateQuality(task);
//                counter++;
//            }
//        }
//        if (counter > 0) {
//            return sum / counter;
//        } else {
//            return -1;
//        }
        return tasks.stream()
                .filter(task -> task.getActualTimeMinutes() > 0 && task.getTimeEstimateMinutes() > 0)
                .mapToDouble(this::calculateQuality)
                .average()
                .orElse(-1);
    }

    private double calculateQuality(Task task) {
        if (task.getTimeEstimateMinutes() > task.getActualTimeMinutes()) {
            return (double) task.getActualTimeMinutes() / task.getTimeEstimateMinutes();
        } else {
            return (double) task.getTimeEstimateMinutes() / task.getActualTimeMinutes();
        }
    }

}
