package net.schoeninger.dashboard.services;

import net.schoeninger.dashboard.domain.Stats;

public interface StatsService {

    Stats getStatsForProject(long projectId);

}
