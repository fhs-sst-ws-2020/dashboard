package net.schoeninger.dashboard.repositories;

import net.schoeninger.dashboard.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class TaskRemoteRepository implements TaskRepository {

    private final RestTemplate restTemplate;

    @Autowired
    public TaskRemoteRepository(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Task> findAllDoneTasksByProjectId(long projectId) {
        ResponseEntity<Task[]> response = restTemplate.getForEntity(
                "http://task-app-service/api/v1/projects/" + projectId + "/tasks?done=true",
                Task[].class
        );
        return response != null && response.getBody() != null ? Arrays.asList(response.getBody()) : null;
    }

}
