package net.schoeninger.dashboard.repositories;

import net.schoeninger.dashboard.domain.Task;

import java.util.List;

public interface TaskRepository {

    List<Task> findAllDoneTasksByProjectId(long projectId);

}
