package net.schoeninger.dashboard.controllers;

import lombok.extern.slf4j.Slf4j;
import net.schoeninger.dashboard.domain.Stats;
import net.schoeninger.dashboard.services.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/dashboard")
public class DashboardRestController {

    private StatsService statsService;

    @Autowired
    public DashboardRestController(StatsService statsService) {
        this.statsService = statsService;
    }

    @GetMapping("/projects/{projectId}")
    public Stats getStatsForProject(@PathVariable long projectId) {
        log.info("Called method with projectId " + projectId);
        return statsService.getStatsForProject(projectId);
    }

}
