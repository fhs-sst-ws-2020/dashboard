# Example multi-stage Dockerfile for Gradle and Java.
# In this example we use 2 stages:
#    - Build & Test: Builds our Java application using gradle and executes the unit tests.
#    - Runtime: Packages the application built in the previous stage together with the Java runtime.
# The advantage of this approach is that the final runtime image doesn't contain the build tools.
# Therefore, the image is smaller and more secure!

#################################################################
# Stage 1: BUILD & TEST                                         #
#################################################################
FROM gradle:6.7.0-jdk8 AS build
# do copy only necessary build files - otherwise e.g. changes
# to the Dockerfile results in a complete rebuild
COPY --chown=gradle:gradle build.gradle gradlew settings.gradle /app/
COPY --chown=gradle:gradle ./gradle /app/gradle
COPY --chown=gradle:gradle ./src /app/src

# set work dir and build
WORKDIR /app/
RUN gradle build --no-daemon

#################################################################
# Stage 2: RUNTIME                                              #
#################################################################
FROM openjdk:8-jre-alpine

EXPOSE 8081

WORKDIR /app

# get script which waits for a service to start-up ("sh"-shell based for alpine container)
# ADD https://raw.githubusercontent.com/eficode/wait-for/master/wait-for /usr/local/bin/
# RUN chmod 755 /usr/local/bin/wait-for

# run as non-root user
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring

# get sources from build stage
COPY --from=build /app/build/libs/*.jar /app/app.jar

# set-up Eureka host - for local execution (without docker-compose)
# if picked up from an environment variable you may reconfigure it
# e.g. in the docker-compose.yml file
ENV EUREKA_HOST=${EUREKA_HOST:-"localhost:8761"}

# wait-for script waits for 180 seconds for the Eureka server to start. When successfull
# it runs the java app.jar application
# ENTRYPOINT /usr/local/bin/wait-for "${EUREKA_HOST}" --timeout=180 -- java -jar /app/app.jar

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
